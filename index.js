const express = require('express')
var bodyParser = require('body-parser')
// const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
// const url = 'mongodb://localhost:27017';
// const dbName = 'usercenter';


const pg = require('pg');
const connectionString = 'postgressql://postgres:password@onedemo.metrosystems.co.th:80/postgres';

const config = 
{
  user: 'postgres',
  host: 'onedemo.metrosystems.co.th',
  database: 'postgres',
  password: 'password',
  port: 80,
  ssl: false
};


const app = express()

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    next();
  });
 
// parse application/json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));



app.get('/', (req, res) => {
  res.send('Hello World')
});

app.get('/GetGraph/:id', (req, res) => {

  var id = req.params.id;
  const client = new pg.Client(config);
  client.connect(err => {
      if (err) throw err;
      else {
  client.query('SELECT * FROM public_b1.retail_comp WHERE item_id = $1 and ( SELECT max(timestamp) FROM public_b1.retail_comp )- date(timestamp) < 7 ;', [id], (error, results) => {
    if (error) {
      throw error
    }
    else{
    
       results.rows.forEach(element => {
            if(element.retail_name == "bigc"){

            }else if(element.retail_name == "lotus"){

            }else if(element.retail_name == "makro"){

            }
            
      });


    res.status(200).json(results.rows)
  }
});

}  

});

});


app.get('/pgcreate', (req,res)=> {
    const client = new pg.Client(config);

    client.connect(err => {
        if (err) throw err;
        else {
            const query = `
        CREATE TABLE userlogin (id serial PRIMARY KEY, firstname VARCHAR(50), lastname VARCHAR(50), image VARCHAR(150),churn BOOLEAN);
        INSERT INTO userlogin (firstname , lastname , image ,churn) VALUES ('benjarat', 'C.', 'https://cdn.pixabay.com/photo/2016/11/08/15/21/user-1808597_960_720.png', false);
        INSERT INTO userlogin (firstname , lastname , image ,churn) VALUES ('jetthapat', 'T.', 'https://cdn.pixabay.com/photo/2016/11/08/15/21/user-1808597_960_720.png', true);
     `;

    client
        .query(query)
        .then(() => {
            console.log('Table created successfully!');
            client.end(console.log('Closed client connection'));
        })
        .catch(err => console.log(err))
        .then(() => {
            console.log('Finished execution, exiting now');
            process.exit();
        });
        }
    });
})

app.get('/testpg',(req,res) => {

    const client = new pg.Client(config);
    client.connect(err => {
        if (err) throw err;
        else {
            console.log(`Running query to PostgreSQL server: ${config.host}`);
            // const query = 'SELECT * FROM userlogin ORDER BY id DESC LIMIT 1;';
            const query = 'SELECT * FROM public_b1.retail_comp;';
        
            client.query(query)
                .then(res => {
                    const rows = res.rows;
        
                    rows.map(row => {
                        console.log(`Read: ${JSON.stringify(row)}`);
                    });
        
                    
                })
                .catch(err => {
                    console.log(err);
                });
                
        }
        
       
    });
    client.end(console.log('Closed client connection'));

});

app.listen(3000, () => {
  console.log('Start server at port 3000.')
})

app.post('/register', (req, res, next) => {
    var firstname = req.body.fname;
    var lastname = req.body.lname;
    var image = req.body.bloblink;
    var churn = req.body.churn;
    var userid = "user1";

    const client = new pg.Client(config);
    client.connect(err => {
        if (err) throw err;
        else {
            console.log(`Running query to PostgreSQL server: ${config.host}`);
            const query = `INSERT INTO userlogin (firstname , lastname , image ,churn) VALUES ($1,$2,$3,$4)`;
            //const query = 'SELECT * FROM public_b1.retail_comp;';
        
            client.query(query,[firstname,lastname,image,churn],(err,result) => {
                if(err){
                    console.log(err);
                }else{

                     client.query('SELECT COUNT(*) as userid FROM userlogin',(err,result) => {
                      if(err){
                        console.log(err);
                      }else{
                         res.status(200).send(result.rows[0]);
                         }
                     })
                   
                }
            })
        }
        
       
    });
  
    // MongoClient.connect(url, function(err, client) {
    //   assert.equal(null, err);
    //   console.log("Connected successfully to server");
     
    //   const db = client.db(dbName);
     
    //   insertDocuments(firstname,lastname,userid,image,churn,db, function() {
    //     client.close();
    //   });
    // });
    // res.send('finish register');
  });

  app.get('/alluser', (req, res, next) => {
    const client = new pg.Client(config);
    client.connect(err => {
        if (err) throw err;
        else {
            console.log(`Running query to PostgreSQL server: ${config.host}`);
            const query = 'SELECT * FROM userlogin ORDER BY id DESC LIMIT 1;';
            //const query = 'SELECT * FROM public_b1.retail_comp;';
        
            client.query(query,(err,result)=>{
                if(err){
                     console.log(err);
                }else{
                    res.status(200).json(result.rows)
                    
        client.end(console.log('Closed client connection'));
                }
            })
                
        }


        
        
       
    });


    // MongoClient.connect(url, function(err, client) {
    //   assert.equal(null, err);
    //   console.log("Connected successfully to server");
     
    //   const db = client.db(dbName);
     
    //   findAllDocuments(db,res, function() {
    //     client.close();
    //   });
    // });
  });

  app.post('/insertbasket', (request, response, next) => {


    const item_code = request.body.item_code
    const userid = request.body.userid
    const price = request.body.price
    const productname = request.body.productname
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    
    const client = new pg.Client(config);
    client.connect(err => {
        if (err) throw err;
        else {
    client.query('SELECT * FROM public_b1.buskets WHERE item_code = $1 and user_id = $2;', [item_code,userid], (error, results1) => {
      if (error) {
        throw error
      }
      //response.send(results1.rows[0])
      if(results1.rows.length > 0){
        var number = results1.rows[0].number
        number =  parseInt(number) + 1
        console.log(dateTime);
        client.query('UPDATE public_b1.buskets SET number = $1 , update_time = $4 WHERE item_code = $2 and user_id = $3;', [number,item_code,userid,dateTime], (error, results2) => {})
        response.status(200).send('update')
      }
    else{
      console.log(dateTime);
      client.query('INSERT INTO public_b1.buskets (item_code,user_id,price,item_name,number,update_time) VALUES ($1, $2, $3, $4, 1, $5)', [item_code, userid,price,productname,dateTime], (error, results3) => {
    if (error) {
      throw error
    }else{
      response.status(200).send('Added')
    }
    })
   }
   })
  }
  })

})

  const findAllItems = function(db,res, callback) {
    // Get the documents collection
    const collection = db.collection('items');
    // Find some documents
  collection.find().toArray(function(err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.log(docs);
    if(docs.length < 1) res.send('Not Found User in DB');
    else {res.json(docs);
    callback(docs);
    }
  });
}

  const findAllDocuments = function(db,res, callback) {
    // Get the documents collection
    const collection = db.collection('users');
    // Find some documents
    collection.find().limit(1).sort({$natural:-1}).toArray(function(err, docs) {
     console.log("Found the following records");
      console.log(docs);
      if(docs.length < 1) res.send('Not Found User in DB');
      else {res.json(docs);
      callback(docs);
      }
    });
  }




  const insertDocuments = function(firstname,lastname,userid,image,churn,db,callback) {
    // Get the documents collection
    const collection = db.collection('users');
    // Insert some documents
    collection.insertMany([
      {firstname : firstname,lastname:lastname,userid:userid,image:image,churn:churn},
    //   {firstname : firstname,lastname:lastname,email:email,userid : userid,username : username,password : password,department : department,position :position, sex :sex},
    //   {firstname : 'benjarat',lastname:'chongpao',email:'benjarat@booking.com',userid : 'benben',username : 'benjarat',password : 'bookbook',department : 'MSC/DTS',position :'DEV', sex :'female'},
    //   {firstname : 'naphol',lastname:'aya',email:'naphol@hotmail.com',userid : 'napholaya',username : 'naphol',password : 'imfine',department : 'MSC/DTS',position :'DEV', sex :'male'},
    //   {firstname : 'nontouch',lastname:'boonyamanond',email:'jeff@hotmail.com',userid : 'jeff123',username : 'nontouch',password : 'xd12345',department : 'MSC/DTS',position :'DEV', sex :'male'},
    //   {firstname : 'jettapat',lastname:'thitaram',email:'nopnopnop@metrosystems.co.th',userid : 'nopu',username : 'jettapat',password : '123789456',department : 'MSC/DTS',position :'DEV', sex :'male'}
    ]
    , function(err, result) {
      assert.equal(err, null);
      assert.equal(1, result.result.n);
      assert.equal(1, result.ops.length);
      console.log("Inserted 5 documents into the collection");
      callback(result);
    });
  }
